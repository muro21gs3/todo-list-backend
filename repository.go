package main

import (
	"context"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Repository struct {
	client *mongo.Client
}

func NewRepository(dbUrl string) *Repository {
	ctx, _ := context.WithTimeout(context.Background(), 15*time.Second)
	clientOptions := options.Client().ApplyURI(dbUrl)
	client, _ := mongo.Connect(ctx, clientOptions)
	return &Repository{client}
}

func (repository *Repository) AddTodoRepository(todoModel *TodoModel) error {
	collection := repository.client.Database("todo").Collection("todolist")
	ctx, _ := context.WithTimeout(context.Background(), 15*time.Second)

	_, err := collection.InsertOne(ctx, todoModel)

	if err != nil {
		return err
	}

	return nil
}

func (repository *Repository) GetTodoListRepository() (*TodoListModel, error) {
	collection := repository.client.Database("todo").Collection("todolist")
	ctx, _ := context.WithTimeout(context.Background(), 15*time.Second)
	cursor, err := collection.Find(ctx, bson.M{})

	defer cursor.Close(ctx)
	todoListModel := TodoListModel{}
	for cursor.Next(ctx) {
		todoModel := TodoModel{}
		cursor.Decode(&todoModel)
		if len(todoModel.Todo) > 0 {
			todoListModel.TodoList = append(todoListModel.TodoList, todoModel)
		}
	}

	if err = cursor.Err(); err != nil {
		return nil, err
	}

	if err != nil {
		return nil, err
	}
	return &todoListModel, nil
}
