package main

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
)

type ServiceConfig struct {
	Port       string
	MongoDBURL string
}

func main() {
	fmt.Println("todo-list service started...")

	config := ServiceConfig{
		Port:       ":8080",
		MongoDBURL: "mongodb://mongo-0.mongo:27017",
	}
	repository := NewRepository(config.MongoDBURL)
	service := NewService(repository)
	api := NewAPI(service)
	app := ServiceSetup(api)

	app.Listen(config.Port)
}

func ServiceSetup(api *Api) *fiber.App {
	app := fiber.New()
	app.Use(cors.New(cors.Config{
		AllowOrigins: "*",
		AllowMethods: "GET,POST,PATCH",
	}))
	app.Use(logger.New())
	app.Get("/todo", api.GetTodoListApi)
	app.Post("/todo", api.PostTodoApi)
	return app
}
