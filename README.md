# Getting Connected with Todo List Backend

### `http://35.203.73.201/` prod host

### `http://34.85.160.252/` test host


### `mongodb setup`

#### kubectl exec -it mongo-0 -- mongo
#### rs.initiate()
#### var cfg = rs.conf()
#### cfg.members[0].host="mongo-0.mongo:27017"
#### rs.reconfig(cfg)
