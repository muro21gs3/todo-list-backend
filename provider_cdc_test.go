package main

import (
	"context"
	"fmt"
	"log"
	"testing"
	"time"

	"github.com/pact-foundation/pact-go/dsl"
	"github.com/pact-foundation/pact-go/types"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func TestProvider_TodoGetPost(t *testing.T) {
	pact := &dsl.Pact{
		Provider: "todo-list-backend",
	}
	var repository *Repository

	port := 3000

	pact.VerifyProvider(t, types.VerifyRequest{
		ProviderBaseURL:            fmt.Sprintf("http://127.0.0.1:%d", port),
		BrokerURL:                  "https://mbayraktar.pactflow.io",
		BrokerToken:                "pJDKUGkLqn2__i1c4R1Mgw",
		PublishVerificationResults: true,
		ProviderVersion:            "asdasdasdas-asd121dasd-12dsadasd",

		BeforeEach: func() error {
			reposi := NewMockRepository()
			Service := NewService(reposi)
			API := NewAPI(Service)
			app := ServiceSetup(API)

			go app.Listen(fmt.Sprint(":", port))

			return nil
		},
		StateHandlers: types.StateHandlers{
			"there is 3 todo": func() error {
				err := repository.AddTodoRepository(&TodoModel{ID: "1", Todo: "test 1"})
				err = repository.AddTodoRepository(&TodoModel{ID: "2", Todo: "test 2"})
				err = repository.AddTodoRepository(&TodoModel{ID: "3", Todo: "test 3"})

				return err
			},
			"there is 1 todo": func() error {
				err := repository.AddTodoRepository(&TodoModel{ID: "4", Todo: "Buy the milk"})
				return err
			},
		}},
	)
}

func NewMockRepository() *Repository {
	clientOptions := options.Client().
		ApplyURI("mongodb://34.72.203.214:27017/?connect=direct")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		log.Fatal(err)
	}
	return &Repository{client}
}
