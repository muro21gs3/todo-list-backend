package main

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"
	"testing"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

func TestMain_TodoPost(t *testing.T) {
	Convey("Given to-do DTO", t, func() {
		repository := GetTestRepository()
		service := NewService(repository)
		api := NewAPI(service)

		todo := TodoDTO{
			Todo: "To-do post request olustur.",
		}

		Convey("When create todo post request", func() {
			todoByte, err := json.Marshal(todo)
			So(err, ShouldBeNil)

			todoReader := bytes.NewReader(todoByte)
			request, err := http.NewRequest(http.MethodPost, "/todo", todoReader)
			So(err, ShouldBeNil)

			request.Header.Add("Content-Type", "application/json")
			request.Header.Set("Content-Length", strconv.Itoa(len(todoByte)))

			app := ServiceSetup(api)
			response, err := app.Test(request, 20000)
			So(err, ShouldBeNil)

			Convey("Then status code should be 201", func() {
				So(response.StatusCode, ShouldEqual, fiber.StatusCreated)
			})
		})
	})
}

func TestMain_TodoListGet(t *testing.T) {
	Convey("Given to-do model in database", t, func() {
		repository := GetTestRepository()
		service := NewService(repository)
		api := NewAPI(service)

		todoID := uuid.New().String()
		todoID2 := uuid.New().String()
		todoID3 := uuid.New().String()

		todoModel := TodoModel{
			ID:        todoID,
			Todo:      "To-do get request olustur.",
			CratedAt:  time.Now().Round(time.Minute).UTC(),
			UpdatedAt: time.Now().Round(time.Minute).UTC(),
		}

		todoModel2 := TodoModel{
			ID:        todoID2,
			Todo:      "To-do get request olustur.2",
			CratedAt:  time.Now().Round(time.Minute).UTC(),
			UpdatedAt: time.Now().Round(time.Minute).UTC(),
		}
		todoModel3 := TodoModel{
			ID:        todoID3,
			Todo:      "To-do get request olustur.2",
			CratedAt:  time.Now().Round(time.Minute).UTC(),
			UpdatedAt: time.Now().Round(time.Minute).UTC(),
		}

		repository.AddTodoRepository(&todoModel)
		repository.AddTodoRepository(&todoModel2)
		repository.AddTodoRepository(&todoModel3)

		Convey("When I get list request", func() {
			request, _ := http.NewRequest(http.MethodGet, "/todo", nil)

			request.Header.Add("Content-Type", "application/json")

			app := ServiceSetup(api)
			response, err := app.Test(request, 30000)
			So(err, ShouldBeNil)

			Convey("Then Status Code Should be 200", func() {
				So(response.StatusCode, ShouldEqual, fiber.StatusOK)

				Convey("Then to-do Should be returned", func() {
					responseBody, err := ioutil.ReadAll(response.Body)
					So(err, ShouldBeNil)

					returnedData := TodoListDTO{}

					err = json.Unmarshal(responseBody, &returnedData)
					So(err, ShouldBeNil)

					So(returnedData.TodoList[len(returnedData.TodoList)-3].ID, ShouldEqual, todoID)
					So(returnedData.TodoList[len(returnedData.TodoList)-2].ID, ShouldEqual, todoID2)
					So(returnedData.TodoList[len(returnedData.TodoList)-1].ID, ShouldEqual, todoID3)
					So(returnedData.TodoList[len(returnedData.TodoList)-3].Todo, ShouldEqual, todoModel.Todo)
					So(returnedData.TodoList[len(returnedData.TodoList)-2].Todo, ShouldEqual, todoModel2.Todo)
					So(returnedData.TodoList[len(returnedData.TodoList)-1].Todo, ShouldEqual, todoModel3.Todo)
				})
			})
		})
	})
}

func GetTestRepository() *Repository {

	config := ServiceConfig{
		Port:       ":8080",
		MongoDBURL: "mongodb://35.239.74.79:27017/?connect=direct",
	}
	repository := NewRepository(config.MongoDBURL)

	return repository
}
