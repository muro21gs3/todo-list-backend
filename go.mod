module example.com/todo-list-backend

go 1.16

require (
	github.com/gofiber/fiber/v2 v2.18.0
	github.com/google/uuid v1.3.0
	github.com/pact-foundation/pact-go v1.6.4
	github.com/smartystreets/goconvey v1.6.4
	go.mongodb.org/mongo-driver v1.7.1
)
