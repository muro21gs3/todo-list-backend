package main

import (
	"github.com/gofiber/fiber/v2"
)

type TodoDTO struct {
	ID   string `json:"id"`
	Todo string `json:"todo"`
}

type TodoListDTO struct {
	TodoList []TodoDTO `json:"todolist"`
}

type Api struct {
	service *Service
}

func NewAPI(service *Service) *Api {
	return &Api{
		service: service,
	}
}

func (api *Api) GetTodoListApi(ctx *fiber.Ctx) error {
	returnedData, err := api.service.GetTodoListService()

	switch err {
	case nil:
		ctx.Status(fiber.StatusOK)
		ctx.JSON(returnedData)
		return nil

	case fiber.ErrBadRequest:
		ctx.Status(fiber.StatusBadRequest)
		return err

	default:
		ctx.Status(fiber.StatusInternalServerError)
		return err
	}
}

func (api *Api) PostTodoApi(ctx *fiber.Ctx) error {
	todoDTO := TodoDTO{}
	ctx.BodyParser(&todoDTO)
	err := api.service.PostTodoService(&todoDTO)

	switch err {
	case nil:
		ctx.Status(fiber.StatusCreated)
		return nil
	case fiber.ErrBadRequest:
		ctx.Status(fiber.StatusBadRequest)
		return err
	default:
		ctx.Status(fiber.StatusInternalServerError)
		return err
	}
}
