package main

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestUnit_TestTools(t *testing.T) {
	Convey("Get Test Repository", t, func() {
		repository := GetTestRepository()
		So(repository, ShouldNotBeNil)
	})
}

func TestUnit_Converters(t *testing.T) {
	Convey("Convert Todo DTO to Model", t, func() {
		todoDTO := TodoDTO{Todo: "test"}
		todoModely := ConvertTodoDTOtoModel(&todoDTO)
		So(todoDTO.Todo, ShouldEqual, todoModely.Todo)
	})

	Convey("Convert Todo Model to DTO", t, func() {
		todoModel := TodoModel{ID: "1", Todo: "test"}
		todoEntity := ConvertTodoModeltoDTO(&todoModel)
		So(todoModel.ID, ShouldEqual, todoEntity.ID)
		So(todoModel.Todo, ShouldEqual, todoEntity.Todo)
	})

	Convey("Convert Todo List Model to DTO", t, func() {
		todoListModel := TodoListModel{TodoList: []TodoModel{{ID: "1", Todo: "test"}, {ID: "2", Todo: "test 2"}}}
		todoListDTO := ConvertTodoListModeltoDTO(&todoListModel)
		So(todoListModel.TodoList[0].ID, ShouldEqual, todoListDTO.TodoList[0].ID)
		So(todoListModel.TodoList[1].ID, ShouldEqual, todoListDTO.TodoList[1].ID)
		So(todoListModel.TodoList[0].Todo, ShouldEqual, todoListDTO.TodoList[0].Todo)
		So(todoListModel.TodoList[1].Todo, ShouldEqual, todoListDTO.TodoList[1].Todo)
	})
}
