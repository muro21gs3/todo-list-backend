package main

import (
	"time"

	"github.com/google/uuid"
)

type TodoModel struct {
	ID        string    `json:"id" bson:"_id"`
	Todo      string    `json:"todo" bson:"todo"`
	CratedAt  time.Time `json:"createdat" bson:"createdat"`
	UpdatedAt time.Time `json:"updatedat" bson:"updatedat"`
}

type TodoListModel struct {
	TodoList []TodoModel `json:"todolist"`
}

type Service struct {
	repository *Repository
}

func NewService(repository *Repository) *Service {
	return &Service{
		repository: repository,
	}
}

func (service *Service) PostTodoService(todoDTO *TodoDTO) error {
	todoModel := ConvertTodoDTOtoModel(todoDTO)
	todoModel.ID = uuid.New().String()
	todoModel.CratedAt = time.Now().Round(time.Minute).UTC()
	todoModel.UpdatedAt = time.Now().Round(time.Minute).UTC()

	err := service.repository.AddTodoRepository(todoModel)
	if err != nil {
		return err
	}

	return nil
}

func (service *Service) GetTodoListService() (*TodoListDTO, error) {
	todoListModel, err := service.repository.GetTodoListRepository()
	if err != nil {
		return nil, err
	}

	todoListDTO := ConvertTodoListModeltoDTO(todoListModel)
	return todoListDTO, nil
}

func ConvertTodoModeltoDTO(todoModel *TodoModel) *TodoDTO {
	todoDTO := TodoDTO{
		ID:   todoModel.ID,
		Todo: todoModel.Todo,
	}
	return &todoDTO
}

func ConvertTodoListModeltoDTO(todoListModel *TodoListModel) *TodoListDTO {
	todoListDTO := TodoListDTO{}
	for _, v := range todoListModel.TodoList {
		todoListDTO.TodoList = append(todoListDTO.TodoList, *ConvertTodoModeltoDTO(&v))
	}
	return &todoListDTO
}

func ConvertTodoDTOtoModel(todoDTO *TodoDTO) *TodoModel {
	todoModel := TodoModel{
		Todo: todoDTO.Todo,
	}
	return &todoModel
}
